package com.ransam.trendingrepo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Sriram-Lobby on 5/24/2017.
 */

public class TrendRepo_DB extends SQLiteOpenHelper {

    public TrendRepo_DB(Context context) {
        super(context, "TrendingRepo.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table value(name text, description text, language text, star text, color text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists value");
        /*if(oldVersion<6){
            db.execSQL("");
        }*/

    }

    public void insert(String name, String description, String language, String star, String color) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("description", description);
        cv.put("language", language);
        cv.put("star", star);
        cv.put("color", color);
        db.insert("value", null, cv);
        db.close();
    }

    public ArrayList<HashMap<String, String>> get_allRepo() {
        ArrayList<HashMap<String, String>> oslist = new ArrayList<>();
        try {
            String selectQuery;
            selectQuery = "SELECT  * FROM value"; // ORDER by name ASC
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("name", cursor.getString(0));
                    map.put("desc", cursor.getString(1));
                    map.put("language", cursor.getString(2));
                    map.put("star", cursor.getString(3));
                    map.put("color", cursor.getString(4));
                    oslist.add(map);
                } while (cursor.moveToNext());
            }

            db.close();
            return oslist;
        } catch (Exception e) {
            Log.i("John Data", e.getMessage());
        }
        return oslist;
    }

    public void deleteRepo() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("value", null, null);
        db.close();

    }

}