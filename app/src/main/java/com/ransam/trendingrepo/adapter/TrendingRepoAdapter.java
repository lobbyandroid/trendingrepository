package com.ransam.trendingrepo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ransam.trendingrepo.R;
import com.ransam.trendingrepo.pojo.RepoResponseModel;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrendingRepoAdapter extends RecyclerView.Adapter<TrendingRepoAdapter.CustomViewHolder> {

        final private ArrayList<HashMap<String, String>> dataList;
        final private Context context;

        public TrendingRepoAdapter(Context context, ArrayList<HashMap<String, String>> dataList){
            this.context = context;
            this.dataList = dataList;
        }

        static class CustomViewHolder extends RecyclerView.ViewHolder {

            public final View mView;

            TextView txtTitle, txtDesc, txtLLanguage, textCount;
            private CircleImageView profileImage;

            CustomViewHolder(View itemView) {
                super(itemView);
                mView = itemView;
                txtTitle = mView.findViewById(R.id.title);
                txtDesc = mView.findViewById(R.id.desc);
                txtLLanguage = mView.findViewById(R.id.language);
                textCount = mView.findViewById(R.id.starCount);
                profileImage = mView.findViewById(R.id.profile_image);
            }
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.adapter_chield, parent, false);
            return new CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            holder.txtTitle.setText(dataList.get(position).get("name"));
            holder.txtLLanguage.setText(dataList.get(position).get("language"));
            holder.txtDesc.setText(dataList.get(position).get("desc"));
            holder.textCount.setText(dataList.get(position).get("star"));//dataList.get(position).getLanguageColor()
            holder.profileImage.setColorFilter(Color.parseColor(dataList.get(position).get("color")));
        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

}
