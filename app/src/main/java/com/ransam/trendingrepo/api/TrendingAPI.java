package com.ransam.trendingrepo.api;

import com.ransam.trendingrepo.pojo.RepoResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface TrendingAPI {

    @GET("repositories")
    Call<List<RepoResponseModel>> getTrendingRepo();
}
