package com.ransam.trendingrepo.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ransam.trendingrepo.R;
import com.ransam.trendingrepo.adapter.TrendingRepoAdapter;
import com.ransam.trendingrepo.database.TrendRepo_DB;
import com.ransam.trendingrepo.pojo.RepoResponseModel;
import com.ransam.trendingrepo.api.TrendingAPI;
import com.ransam.trendingrepo.client.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;
    RecyclerView recyclerView;
    Toolbar toolBar;
    ArrayList<HashMap<String, String>> rsponce = new ArrayList<>();
    ProgressBar progressBar;
    LinearLayout noNetwork, noData;
    Button loadOffline;
    TrendRepo_DB db;
    ArrayList<HashMap<String,String>> data = new ArrayList<>();
    ArrayList<HashMap<String,String>> localData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeLayout = findViewById(R.id.swipeLayout);
        toolBar = findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        progressBar = findViewById(R.id.progressBar);
        noNetwork = findViewById(R.id.noNetwork);
        noData = findViewById(R.id.noData);
        loadOffline = findViewById(R.id.loadOffline);
        recyclerView = findViewById(R.id.recycleView);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        swipeLayout.setOnRefreshListener(this);
        db = new TrendRepo_DB(getApplicationContext());
        localData = db.get_allRepo();
        if(isConnectingToInternet(this)) {
            startRequest();
            noNetwork.setVisibility(View.GONE);
        } else {
            noNetwork.setVisibility(View.VISIBLE);
        }
        loadOffline.setOnClickListener(v -> {
            if(localData.size()>0){
                generateDataList(localData);
                recyclerView.setVisibility(View.VISIBLE);
                noData.setVisibility(View.GONE);
                noNetwork.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                noNetwork.setVisibility(View.GONE);
            }
        });
    }

    private void startRequest() {
        data.clear();
        localData.clear();
        TrendingAPI service = RetrofitClientInstance.getRetrofitInstance().create(TrendingAPI.class);
        Call<List<RepoResponseModel>> call = service.getTrendingRepo();
        call.enqueue(new Callback<List<RepoResponseModel>>() {
            @Override
            public void onResponse(Call<List<RepoResponseModel>> call, Response<List<RepoResponseModel>> response) {
                if(response.body()!=null){
                    if (response.body().size() > 0) {
                        db.deleteRepo();
                        recyclerView.setVisibility(View.VISIBLE);
                        noData.setVisibility(View.GONE);
                        for (int i = 0; i < response.body().size(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("name", response.body().get(i).getName());
                            hashMap.put("desc", response.body().get(i).getDescription());
                            hashMap.put("star", String.valueOf(response.body().get(i).getStars()));
                            hashMap.put("language", response.body().get(i).getLanguage());
                            hashMap.put("color", response.body().get(i).getLanguageColor());
                            data.add(hashMap);
                            localData.add(hashMap);
                            db.insert(response.body().get(i).getName(), response.body().get(i).getDescription(), response.body().get(i).getLanguage(), String.valueOf(response.body().get(i).getStars()), response.body().get(i).getLanguageColor());
                        }
                        generateDataList(data);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                    }
                }
                 swipeLayout.setRefreshing(false);
                 progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<RepoResponseModel>> call, Throwable t) {
                swipeLayout.setRefreshing(false);
                noData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void generateDataList(ArrayList<HashMap<String, String>> data) {
        TrendingRepoAdapter adapter = new TrendingRepoAdapter(this, data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.app_bar_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                rsponce.clear();
                ArrayList<HashMap<String, String>> loadData = new ArrayList<>();
                if(!isConnectingToInternet(MainActivity.this)) {
                    loadData = localData;
                } else {
                    loadData = data;
                }
                if(loadData!=null) {
                    if (loadData.size() > 0) {
                        for (int i = 0; i < loadData.size(); i++) {
                            if (loadData.get(i).get("name").contains(query)) {
                                rsponce.add(rsponce.size(), loadData.get(i));
                            }
                        }
                        if (rsponce.size() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            generateDataList(rsponce);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                }
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                rsponce.clear();
                ArrayList<HashMap<String, String>> loadData = new ArrayList<>();
                if(!isConnectingToInternet(MainActivity.this)) {
                    loadData = localData;
                } else {
                    loadData = data;
                }
                if(loadData!=null) {
                    if (loadData.size() > 0) {
                        for (int i = 0; i < loadData.size(); i++) {
                            if (loadData.get(i).get("name").contains(newText)) {
                                rsponce.add(rsponce.size(), loadData.get(i));
                            }
                        }
                        if (rsponce.size() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            generateDataList(rsponce);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRefresh() {
        recyclerView.setVisibility(View.GONE);
        if(isConnectingToInternet(this)) {
            startRequest();
            noNetwork.setVisibility(View.GONE);
        } else {
            noNetwork.setVisibility(View.VISIBLE);
            swipeLayout.setRefreshing(false);
            noData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}