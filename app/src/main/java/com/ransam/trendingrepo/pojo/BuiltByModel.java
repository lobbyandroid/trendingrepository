package com.ransam.trendingrepo.pojo;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuiltByModel {
    @SerializedName("href")
    private String href;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("username")
    private String username;

    public BuiltByModel(String href, String avatar, String username) {
        this.href = href;
        this.avatar = avatar;
        this.username = username;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
