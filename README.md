libraries used -
	swiperefreshlayout
	circleimageview
	recyclerview
	retrofit
	
Used SQLite for local database.
Used Java language.
Used Retrofit for api calls.
Used ConnectivityManager for check network connection.

These are the permission used -
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	
when there is no internet connection then the last success remote data stored into sqlite database and show it to the user
and if once network available then shows remote data to the user and store it to local databse.